import { immutable } from './immutable';

class Base {
  id: number;
}

class ImmutableBase extends immutable(Base) { }

describe('Immutable', () => {

  it('deve retornar uma nova instância de Immutable', () => {
    const instance = new ImmutableBase({});
    expect(instance instanceof ImmutableBase).toBeTruthy('instanceof Immutable');
    expect(instance instanceof Base).toBeTruthy('instanceof Base');
  });

  it('deve retornar preencher os atributos passados pelo construtor', () => {
    const instance = new ImmutableBase({id: 10});
    expect(instance.id).toEqual(10);
  });

  describe('set', () => {
    it('deve retornar uma nova instância', () => {
      const older = new ImmutableBase({id: 10});
      const newer = older.set('id', 20);
      expect(older).not.toEqual(newer);
      expect(older instanceof ImmutableBase).toBeTruthy('intanceof ImmutableBase');
    });

    it('deve alterar o valor do atributo na nova instância', () => {
      const instance = new ImmutableBase({id: 10}).set('id', 20);
      expect(instance.id).toEqual(20);
    });
  });

});
