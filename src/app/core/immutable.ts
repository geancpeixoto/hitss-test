import { Constructor, EmptyConstructor } from './constructor';

export type ImmutableConstructor<T> = new (p: Partial<T>) => Immutable<T>;

export interface ImmutableMethods<T> {
  set(values: Partial<T>): Immutable<T>;
  set<K extends keyof T>(key: K, value: T[K]): Immutable<T>;
  set<K extends keyof T>(key: K, value: T[K]): Immutable<T>;
}

export type Immutable<T> = Readonly<T> & ImmutableMethods<T>;

export function immutable<T>(base: EmptyConstructor<T>): ImmutableConstructor<T> {
  const c = base as EmptyConstructor<any>;
  return class extends c implements ImmutableMethods<T> {
    constructor(partial: Partial<T>) {
      super();
      Object.assign(this, partial);
    }

    set<K extends keyof T>(key: K | Partial<T>, value?: T[K]): Immutable<T> {
      const constructor = this.constructor as ImmutableConstructor<T>;
      const data = typeof key === 'object' ? { ... this as object, ...key as object } :
        { ... this as object, [key]: value } as any as Partial<T>;
      return new constructor(data);
    }

  } as ImmutableConstructor<T>;
}
