import { AnimationEvent } from '@angular/animations';
import { CdkAccordion, CdkAccordionItem } from '@angular/cdk/accordion';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter,
  Host, Input, OnDestroy, OnInit, Output, ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAccordion, matExpansionAnimations, MatExpansionPanel, MatExpansionPanelState } from '@angular/material';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { ImmutableModel, Model } from '../../shared/model';
import { ModelsService } from '../../shared/models.service';

@Component({
  selector: 'app-model-detail',
  templateUrl: './model-detail.component.html',
  styleUrls: ['./model-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  preserveWhitespaces: false,
  animations: [
    matExpansionAnimations.bodyExpansion,
    matExpansionAnimations.expansionHeaderHeight
  ],
  host: {
    'class': 'app-model-detail',
    '[class.app-model-detail-expanded]': 'expanded',
  }
})
export class ModelDetailComponent extends CdkAccordionItem implements OnDestroy, OnInit {

  @Output() readonly changes = new EventEmitter<ImmutableModel>();

  @ViewChild('header') readonly headerRef: ElementRef;

  @ViewChild('input') readonly modelRef: ElementRef;

  readonly form: FormGroup;

  private _model: ImmutableModel;

  readonly afterClosed = new Subject<AnimationEvent>();

  get src(): string {
    return this.form.get('photo').value;
  }

  @Input('model') set model(m: ImmutableModel) {
    this._model = m;
    this.load(m);

    if (this.isNewer()) {
      // workaround que permite o angular terminar de renderizar o componente antes de abri-lo.
      requestAnimationFrame(() => this.open());
    }
  }

  get model() {
    return this._model;
  }

  constructor(
    @Host() accordion: MatAccordion, fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    uniqueSelectionDispatcher: UniqueSelectionDispatcher,
    private readonly modelServive: ModelsService) {
    super(accordion, changeDetectorRef, uniqueSelectionDispatcher);

    this.form = fb.group({
      code: null,
      model: [null, Validators.required],
      brand: [null, Validators.required],
      price: [null, Validators.min(0)],
      date: null,
      photo: null
    });
  }

  ngOnInit(): void {
    // Quando o status do formulário mudar, atualize a interface. Utilizado para habilitar/desabilitar
    // o botão de salvar
    this.form.statusChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => this.changeDetectorRef.markForCheck());

    // Quando o usuário atualizar o endereço da foto, atualize a interface
    this.form.get('photo').valueChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe(src => this.changeDetectorRef.markForCheck());
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.afterClosed.complete();
  }

  isNewer() {
    return !this.model.code;
  }

  delete() {
    // TODO: Adicionar validação com o usuário

    this.modelServive
      .delete(this.model)
      .subscribe({
        next: updated => this.collapseThenEmitChanges(null),
        error: err => this.onError(err)
      });
  }

  save() {
    this.modelServive
      .save(this._model.set(this.form.value as Partial<Model>))
      .subscribe({
        next: updated => this.collapseThenEmitChanges(updated),
        error: err => this.onError(err)
      });
  }

  cancel() {
    this.afterClosed.pipe(take(1))
      .subscribe(() => this.load(this.model));

    this.close();
  }

  getExpandedState(): MatExpansionPanelState {
    return this.expanded ? 'expanded' : 'collapsed';
  }

  getHeaderElement() {
    return this.headerRef.nativeElement;
  }

  handleAnimationChange(event: AnimationEvent) {
    if (event.toState === 'collapsed') {
      this.afterClosed.next();
      requestAnimationFrame(() => (this.headerRef.nativeElement as HTMLElement).focus());
    } else {
      requestAnimationFrame(() => (this.modelRef.nativeElement as HTMLElement).focus());
    }
  }

  private onError(err: any) {
    // TODO: implementar tratamento de erros
  }

  private collapseThenEmitChanges(model: ImmutableModel) {
    this.afterClosed.pipe(take(1))
      .subscribe(() => this.changes.next(model));

    this.close();
  }

  private load(model: ImmutableModel) {
    const data = {
      code: model.code || null,
      model: model.model || null,
      brand: model.brand || null,
      price: model.price || null,
      date: model.date || null,
      photo: model.photo || null,
    } as Partial<Model>;

    this.form.setValue(data, { emitEvent: false });

    if (model.code) {
      this.form.get('code').disable({ emitEvent: false });
    }
  }
}
