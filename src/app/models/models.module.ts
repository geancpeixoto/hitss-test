import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule, MatCardModule, MatDatepicker, MatDatepickerModule, MatExpansionModule,
  MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatRippleModule, MatToolbarModule
} from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { ModelDetailComponent } from './model-detail/model-detail.component';
import { ModelsRoutingModule } from './models-routing.module';
import { ModelsComponent } from './models.component';
import { ModelPhotoComponent } from './model-photo/model-photo.component';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatRippleModule,
    MatToolbarModule,
    ModelsRoutingModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [ModelsComponent, ModelDetailComponent, ModelPhotoComponent],
})
export class ModelsModule { }
