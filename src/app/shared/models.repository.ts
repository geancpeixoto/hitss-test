import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';
import { Observer } from 'rxjs/Observer';
import { observeOn } from 'rxjs/operators';
import { ImmutableModel, Model } from './model';

@Injectable()
export class ModelsRepository {

  private _loaded: Model[] = [
    new ImmutableModel({
      'price': 1990,
      'code': '653d9688-af26-4ff1-b1a6-58c474fba27f',
      'model': 'Moto G5 Plus',
      'brand': 'Motorola',
      'photo': 'https://images.colombo.com.br/produtos/786480/786480_moto_g5_plus_motorola_XT1683_dourado_7_z.jpg?ims=1000x1000',
      'date': new Date(2015, 11, 26)
    }),
    new ImmutableModel({
      'price': 3990,
      'code': 'd10e528f-9f2c-4271-9dc1-967bef224695​',
      'model': 'IPhone 7 Plus',
      'brand': 'Apple',
      'photo': 'http://www3.claro.com.br/sites/default/files/claro-net-simples_5654b06692637-224x170_565c9453ee1bf.png​',
      'date': new Date(2015, 12, 25)
    }),
    new ImmutableModel({
      'price': 699,
      'code': 'ba59f136-9d1f-42bf-8f4a-826dca8dde5b​',
      'model': 'J7',
      'brand': 'Samsung',
      'photo': 'http://www3.claro.com.br/sites/default/files/claro-net-simples_5654b06692637-224x170_565c9453ee1bf.png​',
      'date': new Date(2015, 11, 1)
    })
  ];

  list(): Observable<Model[]> {
    return of(this._loaded);
  }

  delete(model: Model): Observable<void> {
    return Observable.create((observer: Observer<void>) => {
      this._loaded = this._loaded.filter(i => i.code !== model.code);
      observer.next(null);
      observer.complete();
    });
  }

  update(model: Model): Observable<Model> {
    return Observable.create((observer: Observer<Model>) => {
      this._loaded = this._loaded.map(i => i.code === model.code ? model : i);
      observer.next(model);
      observer.complete();
    });
  }

  create(model: Model): Observable<Model> {
    return Observable.create((observer: Observer<Model>) => {
      const created = model.code ? model : new ImmutableModel(model).set('code', this.generateId());
      this._loaded = [created, ...this._loaded];

      observer.next(created);
      observer.complete();
    });
  }

  generateId(): string {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      const r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}
