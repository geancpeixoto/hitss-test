import { inject, TestBed } from '@angular/core/testing';
import { ModelsRepository } from './models.repository';
import { ModelsService } from './models.service';

describe('ModelsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ModelsRepository,
        ModelsService
      ]
    });
  });

  it('should be created', inject([ModelsService], (service: ModelsService) => {
    expect(service).toBeTruthy();
  }));
});
